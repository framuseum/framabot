<?php
$action = isset($_POST['action']) ? $_POST['action'] : '';

require_once('config.php');
session_start();
if(md5($_SESSION['login'].':'.$_SESSION['pass'])==$login_pass) { // SECURITE : Seul un admin peut écrire dans les fichiers

	$app_tmp = array (
			'name' => (isset($_POST['name'])) ? strtolower(preg_replace('/[^A-Za-z0-9-\-\+]/', '',$_POST['name'])) : '',
			'site' => (isset($_POST['site'])) ? $_POST['site'] : '',
			'fs' => (isset($_POST['fs'])) ? $_POST['fs'] : '',
			'wp' => (isset($_POST['wp'])) ? $_POST['wp'] : '',
			'urltoscan' => (isset($_POST['urltoscan'])) ? $_POST['urltoscan'] : '',
			'pattern' => (isset($_POST['pattern'])) ? $_POST['pattern'] : '',
			// Remplissage des valeurs initiales
			'statut' => 0, 
			'offline' => 0,
			'version' => 0,
			'version_date' => 0,
			'fk' => false,
		);
	
	$app = array();
	$message = '';
	
	$file = @file('cache/apps/'.$app_tmp['name'].'.txt');
	if ($file == false) {
		// Ajouter un logiciel
		if ($action=="add") {
			$app = $app_tmp;
			$app_txt = fopen('cache/apps/'.$app['name'].'.txt', 'wb');
			fwrite($app_txt, json_encode($app));fclose($app_txt);
			$message = 'Le logiciel a été ajouté
';			$log = fopen('log/'.date("Ymd", time()).'.txt', 'a+');
			$log_txt ='[Framabot]['.$app['name'].'] '.$message;
			fwrite($log, $log_txt);fclose($log);
		}
	} else {
		$app = json_decode($file[0], true); // Import des données du cache
		
		if ($action=="edit") {
			// Modifier un logiciel
			// avec les nouvelles valeurs
			$app['site'] = $app_tmp['site'];
			$app['fs'] = $app_tmp['fs'];
			$app['wp'] = $app_tmp['wp'];
			$app['urltoscan'] = $app_tmp['urltoscan'];
			$app['pattern'] = $app_tmp['pattern'];
			$message = 'Le logiciel a été modifié';
		} elseif ($action=="fk") {
			// Mettre un logiciel Framakey
			// en priorité haute
			$app['fk'] = true;
			$message = 'Fait';
		}
		$app_txt = fopen('cache/apps/'.$app['name'].'.txt', 'wb');	
		fwrite($app_txt, json_encode($app));fclose($app_txt);
	}
	$json['message'] = $message;
	echo json_encode($json);
}
?>
