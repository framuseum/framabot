<?php
$search = isset($_GET["s"]) ? strtolower(preg_replace('/[^A-Za-z0-9-\-\_\!]/', '',$_GET["s"])) : '';

if($search=='') { // Firefox Search Plugin
	echo '<?xml version="1.0" encoding="UTF-8"?>
<OpenSearchDescription xmlns="http://a9.com/-/spec/opensearch/1.1/">
<ShortName>Framabot Search</ShortName>
<Description>Rechercher un logiciel avec Framabot</Description>
<Image width="16" height="16">data:image/x-icon;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAACXBIWXMAAA3XAAAN1wFCKJt4AAACaklEQVQ4y4XTO2idBRQH8N/57g1KAwqlwUHc1KQPO/leWqhUmkQsDsVFzBBsk3sLWl+1tiGUWFoFwYSvtDpoEFcVbG5aTXTxQVUoVLFicPAxSAUHH1Fj8h2H3pYkDv6nw+F/Hv/zCKvQbE7dkpkPE9txAxbIn4nZooiTExO9Xy3nx2Vj797WVVVlnBxs+88T32VmFeFmbMAScbyr6/cnRkd3LVxJMDDwwdWdnX9Ok1txqlar9o2P3ze3vNLQ0HR3RHU0wk5ytqtrvnd0dNdCHTo75yewlThWljueITIPu0O4VUiVT2JkxznygWazdSQz9l+82PkShmJoaHpzUVTnMFWWvffnaFyr7g30rhrP69bYHfvyr+Hh1lSE7UURm4uiyEEUmfk0kerebAf/iteEt/EPHjLvOJERtSdRy8zBaDSmLpBLZdm/KQ+7TeGM9BvuiUPmIMfcjg9Rl7rjkLlGY+prcrGO6ylmIEZ8hrWrVxsHfZpj3se9CndhLtOFiNhWkBGR6f+xghMRCXXix0w97Vbvxkf4RbrzioQjtqhs49JG2vl6yO8LvIsNzeY7PXHQx5jGWuHzHPNqPuctlRl0YPKS/tMbsR7vFUtL+QqqzOIFMix6EC1cgwFpJ2qYtMYwGSw+j8VML0f7/stMw+SRsux/FlYcUmEmDviGjOHh1tEIT6Esy75mHRYWrnu0o+OnbuJAo9FaT+6Pkb6zOHt5aI3G6Y0RrWOZ+sjZoige/88zZeaLmXa3W/4i07cRarixrXkpwsl16/54bMUzLceePac21WrFI+QW3IS/8UNmnKmqavLEif4vl/P/BVSD/7yzv7YYAAAAAElFTkSuQmCC</Image>
<Url type="application/x-suggestions+json" method="GET" template="http://'.$_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME'].'?s={searchTerms}"></Url>
<Url type="text/html" template="http://'.$_SERVER['SERVER_NAME'].str_replace('search.php','', $_SERVER['SCRIPT_NAME']).'?s={searchTerms}"></Url>
<SearchForm>http://'.$_SERVER['SERVER_NAME'].str_replace('search.php','', $_SERVER['SCRIPT_NAME']).'</SearchForm>
</OpenSearchDescription>';
} else {
	$file = file('cache/framabot.txt');
	$apps = json_decode($file[0], true);

	$suggest = array($search,array());
	$i=0;
	foreach ($apps as $app) {
		if(isset($app['name']) && (preg_match('#'.$search.'#', $app['name'], $matches)=== 1) && $i<10) {
			array_push($suggest[1],$app['name']);
			$i++;
		}
	}
	if(count($suggest[1])==1) {
		array_push($suggest[1],$suggest[1][0].' !f',$suggest[1][0].' !w',$suggest[1][0].' !g',$suggest[1][0].' !k',$suggest[1][0].' !tk',$suggest[1][0].' !tp');
		$suggest[1][0]=$suggest[1][0].' !b';
	}
	if(count($suggest[1])==0) {
		$suggest[1][0]=$search.' !g';
	}
	header('Content-type: application/json');
	echo str_replace('}',']',str_replace('{','[',json_encode($suggest)));
}
?>
